const inventory = require("./inventory");

function sortCarModels()
{
    let sortedCarModels=[];
    for(let i=0;i<inventory.length;i++)
    {
        sortedCarModels.push(inventory[i].car_model)
    }
    return sortedCarModels.sort();
}
module.exports = sortCarModels;