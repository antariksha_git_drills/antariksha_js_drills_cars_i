const inventory = require("./inventory");

function sortCarYear()
{
    let sortedCarYear=[];
    for(let i=0;i<inventory.length;i++)
    {
        sortedCarYear.push(inventory[i].car_year)
    }
    return sortedCarYear.sort();
}
module.exports = sortCarYear;