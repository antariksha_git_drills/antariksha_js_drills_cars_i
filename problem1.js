const inventory = require("./inventory");

function searchID(carID)
{
    for(let i=0; i<inventory.length; i++)
    {
        if(inventory[i].id===carID)
        {
            return `Car ${carID} is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`
        }
    }
    return null
}
module.exports = searchID;