const sortedCarYear = require("./problem4.js") 
const carYear = sortedCarYear();
function olderThanYear2000()
{
    var olderCar=0;
    for(let i=0;i<carYear.length;i++)
    {
        if(carYear[i]>=2000)
        {
            olderCar=i;
            break;
        }
    }
    return olderCar;
}
module.exports = olderThanYear2000;