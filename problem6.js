const inventory = require("./inventory.js")

function AudiBMW()
{
    let carAudiBMW = []
    for(let i=0;i<inventory.length;i++)
    {
        if(inventory[i].car_make==='Audi' || inventory[i].car_make==='BMW')
            carAudiBMW.push(inventory[i]);
    }
    return carAudiBMW;
}

module.exports = AudiBMW; 